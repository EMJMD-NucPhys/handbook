# Handbook

The present handbook serves as a compilation of resources, information, and opportunities for NucPhys students.

## Internships, Opportunities, and Summer Schools/Courses

- European Master of Science in Nuclear Engineering
    - [Website](https://enen.eu/index.php/emsne-certification/)
- ENEN Nuclear Summer School
    - [Website](https://summerschool2020.enen.bme.hu/)
- Culham Plasma Physics Summer School
    - [Website](https://culhamsummerschool.org.uk/)
- Applications of X-ray and Neutron Scattering in Biology, Chemistry and Physics
    - [Website](https://kurser.ku.dk/course/nfyk12004u)
- DESY Summer Student Programme
    - [Website](https://summerstudents.desy.de/)
- International Summer Student Program at GSI-FAIR
    - [Website](https://theory.gsi.de/stud-pro/)
- Euroschool on Exotic Beams
    - [Website](https://www.euroschoolonexoticbeams.be/)
- Summer School about SMRs
    - [Website](http://www.nuclearenergy.polimi.it/elsmor2022ss/)
- Summer School about Neutrons
    - [Website](https://enen.eu/index.php/2022/06/20/hispanos-hands-on-school-on-the-production-detection-and-use-of-neutron-beams/)

## Events, Workshops, and Masterclasses

- World Nuclear Exhibition
    - [Website](https://www.world-nuclear-exhibition.com/en-gb.html)

## Associations and Learned Societies

- European Nuclear Education Network Association
    - [Website](https://enen.eu/)
- Institute of Physics
    - [Website](https://www.iop.org/)
- Institute of Electrical and Electronics Engineers
    - [Website](https://www.ieee.org/)
- IEEE Nuclear and Plasma Sciences Society
    - [Website](https://ieee-npss.org/)
- European Physical Society
    - [Website](https://www.eps.org/)

## Laboratories and Research Groups
